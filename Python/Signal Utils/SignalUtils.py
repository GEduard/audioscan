import librosa
import numpy as np

class Signal:

    def __init__(self):
        self.signalLoaded = False
    #enddef

    def timeSequence(self, sampleRate, timeDuration, timeIncrement):
        samplesNumber = sampleRate * timeDuration
        return np.arange(samplesNumber) * timeIncrement
    #enddef

    def loadWavFile(self, fileName):
        self.yAxis, self.sampleRate = librosa.load(fileName, sr=None)
        self.timeAxis = self.timeSequence(self.sampleRate, 1.0, 1 / self.sampleRate)
        self.mfcc = librosa.feature.mfcc(self.yAxis, self.sampleRate)
        self.signalLoaded = True
    #enddef
#endclass