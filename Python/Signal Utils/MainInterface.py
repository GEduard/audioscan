# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.9.2
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import QFileDialog, QMessageBox
import pyqtgraph as pg
import SignalUtils as sutils
import NetworkUtils as nutils
import sounddevice as sd
import subprocess as sp
import librosa.display
import matplotlib.pyplot as plt
from pyqtgraph import PlotWidget, plot

class Ui_MainWindow(object):

    def openWAVFile(self):
        options = QFileDialog.Options()
        self.fileName = QFileDialog.getOpenFileName(MainWindow, "Sound Explorer", "", "WAV Files (*.wav)", options=options)
        if self.fileName:
            self.signal.loadWavFile(self.fileName[0])
            self.plotSignal()
    #enddef

    def plotSignal(self):
        self.plotView.clear()
        pen = pg.mkPen(color=(255, 0, 0))
        dropListValue = str(self.comboBox.currentText())
        if dropListValue == "Time":
            self.plotView.plot(self.signal.timeAxis, self.signal.yAxis, pen=pen)
        elif dropListValue == "MFCC":
            plt.figure(figsize=(10, 4))
            librosa.display.specshow(self.signal.mfcc, x_axis='time')
            plt.colorbar()
            plt.title('MFCC')
            plt.tight_layout()
            plt.show()
    #enddef

    def testNetwork(self):
        if self.signal.signalLoaded == True:
            mfcc_asNParray = sutils.np.asarray(self.signal.mfcc)
            mfccFlattened = mfcc_asNParray.flatten()
            mfccVector = mfccFlattened[sutils.np.newaxis, :]
            result = self.network.testWav(mfccVector)

            msg = QMessageBox()
            msg.setIcon(QMessageBox.Information)
            if result[0] < 1 - result[0]:
                msg.setText("WAV File: Class A")
            else:
                msg.setText("WAV File: Class B")
            msg.setInformativeText("File Name: " + self.fileName[0])
            msg.setWindowTitle("Network Output")
            msg.setStandardButtons(QMessageBox.Ok)
            retval = msg.exec_()

    #endddef

    def viewOBJFile(self):
        options = QFileDialog.Options()
        objPath = QFileDialog.getOpenFileName(MainWindow, "OBJ Explorer", "", "OBJ Files (*.obj)", options=options)
        if objPath:
            sp.run(["C:/Users/Eduard Gothard/Documents/ChronoMeshLoader/build/Debug/MeshLoader.exe", objPath[0]])

    def playLoadedSound(self):
        if self.signal.signalLoaded == True:
            sd.play(self.signal.yAxis, self.signal.sampleRate)
    #enddef

    def setupUi(self, MainWindow):

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1280, 720)

        self.signal = sutils.Signal()
        self.network = nutils.NeuralNet()
        self.centralWidget = QtWidgets.QWidget(MainWindow)
        self.centralWidget.setLayoutDirection(QtCore.Qt.LeftToRight)
        self.centralWidget.setObjectName("centralWidget")
        
        self.gridLayout_2 = QtWidgets.QGridLayout(self.centralWidget)
        self.gridLayout_2.setContentsMargins(11, 11, 11, 11)
        self.gridLayout_2.setSpacing(6)
        self.gridLayout_2.setObjectName("gridLayout_2")
        
        self.comboBox = QtWidgets.QComboBox(self.centralWidget)
        self.comboBox.setObjectName("comboBox")
        self.comboBox.addItem("")
        self.comboBox.addItem("")

        self.gridLayout_2.addWidget(self.comboBox, 3, 1, 1, 1)
        self.plotButton = QtWidgets.QPushButton(self.centralWidget)
        self.plotButton.clicked.connect(self.plotSignal)
        self.plotButton.setObjectName("plotButton")

        self.gridLayout_2.addWidget(self.plotButton, 3, 0, 1, 1)
        self.testButton = QtWidgets.QPushButton(self.centralWidget)
        self.testButton.clicked.connect(self.testNetwork)
        self.testButton.setObjectName("testButton")

        self.gridLayout_2.addWidget(self.testButton, 1, 4, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(100, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.gridLayout_2.addItem(spacerItem, 3, 2, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.gridLayout_2.addItem(spacerItem1, 3, 4, 1, 1)
        self.openButton = QtWidgets.QPushButton(self.centralWidget)
        self.openButton.setObjectName("openButton")
        self.openButton.clicked.connect(self.openWAVFile)
        self.gridLayout_2.addWidget(self.openButton, 1, 0, 1, 1)

        self.listenButton = QtWidgets.QPushButton(self.centralWidget)
        self.listenButton.setObjectName("listenButton")
        self.listenButton.clicked.connect(self.playLoadedSound)
        self.gridLayout_2.addWidget(self.listenButton, 2, 0, 1, 1)

        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setSpacing(6)
        self.gridLayout.setObjectName("gridLayout")

        self.plotView = pg.PlotWidget(self.centralWidget)
        self.plotView.setObjectName("plotView")
        self.plotView.setBackground('w')
        self.gridLayout.addWidget(self.plotView, 0, 0, 1, 1)

        self.gridLayout_2.addLayout(self.gridLayout, 0, 0, 1, 5)
        spacerItem2 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.gridLayout_2.addItem(spacerItem2, 3, 3, 1, 1)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)

        self.gridLayout_2.addItem(spacerItem3, 1, 1, 1, 3)
        self.loadOBjButton = QtWidgets.QPushButton(self.centralWidget)
        self.loadOBjButton.clicked.connect(self.viewOBJFile)
        self.loadOBjButton.setObjectName("loadOBjButton")

        self.gridLayout_2.addWidget(self.loadOBjButton, 2, 4, 1, 1)
        MainWindow.setCentralWidget(self.centralWidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Signal Utils"))
        self.comboBox.setItemText(0, _translate("MainWindow", "Time"))
        self.comboBox.setItemText(1, _translate("MainWindow", "MFCC"))
        self.plotButton.setText(_translate("MainWindow", "Plot"))
        self.testButton.setText(_translate("MainWindow", "Test Network"))
        self.openButton.setText(_translate("MainWindow", "Open"))
        self.listenButton.setText(_translate("MainWindow", "Listen"))
        self.loadOBjButton.setText(_translate("MainWindow", "View OBJ"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())

