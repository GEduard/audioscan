import tensorflow as tf

class NeuralNet:
    def __init__(self):
        self.model = tf.keras.models.Sequential()
        self.model.add(tf.keras.layers.Dense(640, activation=tf.nn.relu))
        self.model.add(tf.keras.layers.Dense(1600, activation=tf.nn.relu))
        self.model.add(tf.keras.layers.Dense(800, activation=tf.nn.relu))
        self.model.add(tf.keras.layers.Dense(400, activation=tf.nn.relu))
        self.model.add(tf.keras.layers.Dense(200, activation=tf.nn.relu))
        self.model.add(tf.keras.layers.Dense(1, activation=tf.nn.sigmoid))
        self.model.compile(optimizer=tf.keras.optimizers.SGD(lr=0.001, momentum=1e-08), loss='binary_crossentropy', metrics=['accuracy'])
        self.model.load_weights('mfcc_weights/mfcc_weights')
    #enddef

    def testWav(self, mfccVector):
        result = self.model.predict(mfccVector)
        return result
    #enddef
#endclass