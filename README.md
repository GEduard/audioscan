# AudioScan

This project's purpose is to create an object scanner that is based solely on sound.

The initial way we want to achieve this is to have a box instrumented with microphones and a sound source.
We then place an object inside the box and play a basic sound (sum of sine waves). Here's where an AI agent comes in.
After this, we feed a neural network (as numerical input) the reflections picked up by the microphones.
The output of the neural network should be coordinates that can be used to build a mesh to have a 3D representation of the object.