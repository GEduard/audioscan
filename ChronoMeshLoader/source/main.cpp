#include "RenderingSystem.h"
#include "ObjectLoader.h"


int main(int argc, char* argv[]) 
{
	chrono::SetChronoDataPath(CHRONO_DATA_DIR);

	RenderingSystem lv_RenderingSystem;
	ObjectLoader lv_ObjNavigator;

	auto lv_Object = lv_ObjNavigator.LoadUpObject(argv[1]);
	lv_RenderingSystem.AddObject(lv_Object);

	lv_RenderingSystem.StartSimulation();
	return 0;
}