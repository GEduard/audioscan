#include "ObjectLoader.h"

std::shared_ptr<ChPhysicsItem> ObjectLoader::LoadUpObject(const std::string& ac_szObjectPath)
{
	mv_szObjectPath = ac_szObjectPath;

	mv_chBody = std::make_shared<chrono::ChBodyAuxRef>();

	SetObjectParameters();

	SetObjectTexture();

	SetObjectMesh();

	mv_chBody->SetBodyFixed(true);

	return mv_chBody;
}

void ObjectLoader::SetObjectParameters()
{
	mv_chBody->SetMass(15);
	mv_chBody->SetPos(chrono::ChVector<double>(0, 0, 0));
	mv_chBody->SetInertiaXX(chrono::ChVector<double>(0.25, 0.40, 0.40));
	mv_chBody->SetInertiaXY(chrono::ChVector<double>(0.05, 0.05, -0.05));
	mv_chBody->SetFrame_COG_to_REF(chrono::ChFrame<double>(chrono::ChVector<double>(-0.03, 0.15, -0.04), chrono::ChQuaternion<double>(1, 0, 0, 0)));
}

void ObjectLoader::SetObjectTexture()
{
	auto lv_chTexture = std::make_shared<ChTexture>();
	lv_chTexture->SetTextureFilename(GetChronoDataFile("redwhite.png"));
	mv_chBody->AddAsset(lv_chTexture);
}

void ObjectLoader::SetObjectMesh()
{
	auto lv_chVisualizationMesh = std::make_shared<chrono::geometry::ChTriangleMeshConnected>();
	lv_chVisualizationMesh->LoadWavefrontMesh(mv_szObjectPath);
	lv_chVisualizationMesh->Transform(chrono::ChVector<double>(0, 0, 0), chrono::ChMatrix33<double>(0.035));

	auto lv_chVisualizationShape = std::make_shared<chrono::ChTriangleMeshShape>();
	lv_chVisualizationShape->SetMesh(lv_chVisualizationMesh);
	mv_chBody->AddAsset(lv_chVisualizationShape);
}
