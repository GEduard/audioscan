#include "RenderingSystem.h"

RenderingSystem::RenderingSystem() :
	mv_chSystem(),
	mv_chApp(&mv_chSystem, L"Irrlicht FEM visualization", core::dimension2d<u32>(800, 600), false, true)
{
	;
}

void RenderingSystem::AddObject(const std::shared_ptr<ChPhysicsItem>& ac_chItem)
{
	mv_chSystem.Add(ac_chItem);
}

void RenderingSystem::StartSimulation()
{
	InitializeSimulation();

	while (mv_chApp.GetDevice()->run()) 
	{
		mv_chApp.BeginScene();
		mv_chApp.DrawAll();
		mv_chApp.DoStep();
		mv_chApp.EndScene();
	}
}

void RenderingSystem::InitializeSimulation()
{
	mv_chApp.AssetBindAll();
	mv_chApp.AssetUpdateAll();

	mv_chApp.AddTypicalLogo();
	mv_chApp.AddTypicalSky();
	mv_chApp.AddTypicalLights();
	mv_chApp.AddTypicalCamera(core::vector3df(1.5, 3.5, 1.5), core::vector3df(0, 0, 0));
}
