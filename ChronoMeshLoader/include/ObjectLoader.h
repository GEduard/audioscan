#include <memory>
#include <string>

#include "chrono/physics/ChSystemNSC.h"
#include "chrono/assets/ChTexture.h"
#include "chrono/assets/ChTriangleMeshShape.h"
#include "chrono/geometry/ChTriangleMeshConnected.h"

using namespace chrono;

class ObjectLoader
{
	public:
		ObjectLoader() = default;

		std::shared_ptr<ChPhysicsItem> LoadUpObject(const std::string&);

		~ObjectLoader() = default;

	private:
		void SetObjectParameters();

		void SetObjectTexture();

		void SetObjectMesh();

		std::string mv_szObjectPath;
		std::shared_ptr<ChBodyAuxRef> mv_chBody;
};