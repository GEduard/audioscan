#include "chrono_irrlicht/ChIrrApp.h"
#include "chrono/physics/ChSystemNSC.h"

using namespace chrono;
using namespace chrono::fea;
using namespace chrono::irrlicht;
using namespace irr;

class RenderingSystem
{
	public:
		RenderingSystem();

		void AddObject(const std::shared_ptr<ChPhysicsItem>&);

		void StartSimulation();

		~RenderingSystem() = default;

	private:
		void InitializeSimulation();

		ChIrrApp mv_chApp;
		ChSystemNSC mv_chSystem;
};